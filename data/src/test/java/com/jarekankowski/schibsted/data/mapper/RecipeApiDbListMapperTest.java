package com.jarekankowski.schibsted.data.mapper;

import com.jarekankowski.schibsted.data.api.model.ApiRecipe;
import com.jarekankowski.schibsted.data.api.model.ApiRecipeList;
import com.jarekankowski.schibsted.data.db.model.Recipe;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class RecipeApiDbListMapperTest {

    private static final String RECIPE_ID = "recipeId";

    private RecipeApiDbListMapper mRecipeApiDbListMapper;

    @Mock
    private RecipeApiDbMapper     mRecipeApiDbMapper;

    @Mock
    private ApiRecipeList         mApiRecipeList;

    @Mock
    private ApiRecipe             mApiRecipe;

    @Mock
    private Recipe                mRecipe;

    private ArrayList<ApiRecipe>  mApiRecipes;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);
        initMocks();
        mRecipeApiDbListMapper = new RecipeApiDbListMapper(mRecipeApiDbMapper);
    }

    @Test
    public void testOnLowerToUpperLayer()
            throws Exception {
        final List<Recipe> recipes = mRecipeApiDbListMapper.lowerToUpperLayer(mApiRecipeList);

        assertEquals(recipes.size(), mApiRecipes.size());

        final Recipe recipe = recipes.get(0);

        assertEquals(recipe.getId(), mApiRecipe.getRecipeId());
    }

    private void initMocks() {
        when(mApiRecipe.getRecipeId()).thenReturn(RECIPE_ID);
        when(mRecipe.getId()).thenReturn(RECIPE_ID);

        mApiRecipes = new ArrayList<>();
        mApiRecipes.add(mApiRecipe);
        when(mApiRecipeList.getApiRecipeList()).thenReturn(mApiRecipes);

        when(mRecipeApiDbMapper.lowerToUpperLayer(any(ApiRecipe.class))).thenReturn(mRecipe);
    }
}