package com.jarekankowski.schibsted.data.mapper;

import com.jarekankowski.schibsted.data.api.model.ApiRecipe;
import com.jarekankowski.schibsted.data.db.model.Recipe;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class RecipeApiDbMapperTest {

    private RecipeApiDbMapper mRecipeApiDbMapper;

    @Mock
    private ApiRecipe         mApiRecipe;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);
        initMocks();
        mRecipeApiDbMapper = new RecipeApiDbMapper();
    }

    @Test
    public void testOnLowerToUpperLayer()
            throws Exception {
        final Recipe recipe = mRecipeApiDbMapper.lowerToUpperLayer(mApiRecipe);

        assertEquals(recipe.getId(), mApiRecipe.getRecipeId());
        assertEquals(recipe.getUrl(), mApiRecipe.getImageUrl());
        assertEquals(recipe.getName(), mApiRecipe.getTitle());
        assertEquals(recipe.getInstructionsUrl(), mApiRecipe.getF2FUrl());
        assertEquals(recipe.getOriginalUrl(), mApiRecipe.getSourceUrl());
        assertEquals(recipe.getPublisherName(), mApiRecipe.getPublisher());
        assertEquals(recipe.getSocialRank(), mApiRecipe.getSocialRank());
    }

    private void initMocks() {
        when(mApiRecipe.getRecipeId()).thenReturn("recipeId");
        when(mApiRecipe.getImageUrl()).thenReturn("imageUrl");
        when(mApiRecipe.getTitle()).thenReturn("title");
        when(mApiRecipe.getF2FUrl()).thenReturn("f2f");
        when(mApiRecipe.getSourceUrl()).thenReturn("source");
        when(mApiRecipe.getPublisher()).thenReturn("publisher");
        when(mApiRecipe.getSocialRank()).thenReturn("socialRank");
    }
}