package com.jarekankowski.schibsted.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class ApiRecipe {

    @SerializedName("recipe_id")
    private String       mRecipeId;

    @SerializedName("image_url")
    private String       mImageUrl;

    @SerializedName("source_url")
    private String       mSourceUrl;

    @SerializedName("f2f_url")
    private String       mF2FUrl;

    @SerializedName("title")
    private String       mTitle;

    @SerializedName("publisher")
    private String       mPublisher;

    @SerializedName("publisher_url")
    private String       mPublisherUrl;

    @SerializedName("social_rank")
    private String       mSocialRank;

    @SerializedName("ingredients")
    private List<String> mIngredients;

    public String getRecipeId() {
        return mRecipeId;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getSourceUrl() {
        return mSourceUrl;
    }

    public String getF2FUrl() {
        return mF2FUrl;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getPublisher() {
        return mPublisher;
    }

    public String getPublisherUrl() {
        return mPublisherUrl;
    }

    public String getSocialRank() {
        return mSocialRank;
    }

    public List<String> getIngredients() {
        return mIngredients;
    }
}
