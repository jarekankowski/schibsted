package com.jarekankowski.schibsted.data.mapper;

import com.jarekankowski.schibsted.data.api.model.ApiRecipe;
import com.jarekankowski.schibsted.data.api.model.ApiRecipeList;
import com.jarekankowski.schibsted.data.db.model.Recipe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class RecipeApiDbListMapper
        implements Mapper<ApiRecipeList, List<Recipe>> {

    private final RecipeApiDbMapper mRecipeApiDbMapper;

    public RecipeApiDbListMapper(RecipeApiDbMapper recipeApiDbMapper) {
        mRecipeApiDbMapper = recipeApiDbMapper;
    }

    @Override
    public List<Recipe> lowerToUpperLayer(ApiRecipeList apiRecipeList) {
        final List<ApiRecipe> apiRecipes = apiRecipeList.getApiRecipeList();
        final ArrayList<Recipe> recipes = new ArrayList<>(apiRecipes.size());
        for (final ApiRecipe apiRecipe : apiRecipes) {
            recipes.add(mRecipeApiDbMapper.lowerToUpperLayer(apiRecipe));
        }
        return recipes;
    }
}
