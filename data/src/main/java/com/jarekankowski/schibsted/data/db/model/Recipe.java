package com.jarekankowski.schibsted.data.db.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class Recipe
        implements Serializable {

    private final String            mId;

    private final String            mUrl;

    private final String            mName;

    private final String            mInstructionsUrl;

    private final String            mOriginalUrl;

    private final String            mPublisherName;

    private final String            mSocialRank;

    private final ArrayList<String> mIngredients;

    public Recipe(String id, String url, String name, String instructionsUrl, String originalUrl, String publisherName, String socialRank, List<String> ingredients) {
        mId = id;
        mUrl = url;
        mName = name;
        mInstructionsUrl = instructionsUrl;
        mOriginalUrl = originalUrl;
        mPublisherName = publisherName;
        mSocialRank = socialRank;
        mIngredients = new ArrayList<>(ingredients);
    }

    public String getId() {
        return mId;
    }

    public String getUrl() {
        return mUrl;
    }

    public String getName() {
        return mName;
    }

    public String getInstructionsUrl() {
        return mInstructionsUrl;
    }

    public String getOriginalUrl() {
        return mOriginalUrl;
    }

    public String getPublisherName() {
        return mPublisherName;
    }

    public String getSocialRank() {
        return mSocialRank;
    }

    public ArrayList<String> getIngredients() {
        return mIngredients;
    }


}
