package com.jarekankowski.schibsted.data.mapper;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public interface Mapper<LoverLayer, UpperLayer> {

    UpperLayer lowerToUpperLayer(LoverLayer object);
}
