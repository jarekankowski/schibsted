package com.jarekankowski.schibsted.data.mapper;

import com.jarekankowski.schibsted.data.api.model.ApiRecipe;
import com.jarekankowski.schibsted.data.db.model.Recipe;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class RecipeApiDbMapper
        implements Mapper<ApiRecipe, Recipe> {

    @Override
    public Recipe lowerToUpperLayer(ApiRecipe apiRecipe) {
        final String recipeId = apiRecipe.getRecipeId();
        final String imageUrl = apiRecipe.getImageUrl();
        final String title = apiRecipe.getTitle();
        final String f2FUrl = apiRecipe.getF2FUrl();
        final String sourceUrl = apiRecipe.getSourceUrl();
        final String publisher = apiRecipe.getPublisher();
        final String socialRank = apiRecipe.getSocialRank();
        List<String> ingredients = apiRecipe.getIngredients();
        if (CollectionUtils.isEmpty(ingredients)) {
            ingredients = new ArrayList<>();
        }
        return new Recipe(recipeId, imageUrl, title, f2FUrl, sourceUrl, publisher, socialRank, ingredients);
    }
}
