package com.jarekankowski.schibsted.data.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class ApiRecipeDetails {

    @SerializedName("recipe")
    private ApiRecipe mRecipe;

    public ApiRecipe getRecipe() {
        return mRecipe;
    }
}
