package com.jarekankowski.schibsted.data.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class ApiRecipeList {

    @SerializedName("count")
    private int mCount;

    @SerializedName("recipes")
    List<ApiRecipe> mApiRecipeList;

    public int getCount() {
        return mCount;
    }

    public List<ApiRecipe> getApiRecipeList() {
        return mApiRecipeList;
    }
}
