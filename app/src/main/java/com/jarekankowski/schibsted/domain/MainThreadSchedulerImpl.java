package com.jarekankowski.schibsted.domain;

import com.jarekankowski.schibsted.domain.threads.MainThreadScheduler;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by jarekankowski on 15/07/2016.
 */
public class MainThreadSchedulerImpl
        implements MainThreadScheduler {

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
