package com.jarekankowski.schibsted.domain;

import com.jarekankowski.schibsted.domain.threads.BackgroundThreadScheduler;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by jarekankowski on 15/07/2016.
 */
public class BackgroundThreadSchedulerImpl
        implements BackgroundThreadScheduler {

    @Override
    public Scheduler getScheduler() {
        return Schedulers.io();
    }
}

