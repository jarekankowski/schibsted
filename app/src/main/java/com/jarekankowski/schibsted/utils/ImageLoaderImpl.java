package com.jarekankowski.schibsted.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

/**
 * Created by jarekankowski on 28/08/2016.
 */
public class ImageLoaderImpl
        implements ImageLoader {

    private final RequestManager              mGlide;

    private final RoundedSquareTransformation mRoundedSquareTransformation;

    public ImageLoaderImpl(Context context) {
        mGlide = Glide.with(context);
        mRoundedSquareTransformation = new RoundedSquareTransformation(context);
    }

    @Override
    public void loadRoundedSquareImage(String imageUrl, ImageView imageView) {
        mGlide.load(imageUrl)
              .bitmapTransform(mRoundedSquareTransformation)
              .into(imageView);
    }
}
