package com.jarekankowski.schibsted.utils;

import android.widget.ImageView;

/**
 * Created by jarekankowski on 28/08/2016.
 */
public interface ImageLoader {
    void loadRoundedSquareImage(String imageUrl, ImageView imageView);
}
