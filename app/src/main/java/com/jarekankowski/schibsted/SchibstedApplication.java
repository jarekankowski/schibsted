package com.jarekankowski.schibsted;

import android.app.Application;

import com.jarekankowski.schibsted.di.ApplicationModule;

import dagger.ObjectGraph;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class SchibstedApplication
        extends Application {

    private ObjectGraph mObjectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        mObjectGraph = ObjectGraph.create(new ApplicationModule(this));
        mObjectGraph.inject(this);
    }

    public ObjectGraph extendScopedGraph(Object... modules) {
        return mObjectGraph.plus(modules);
    }
}
