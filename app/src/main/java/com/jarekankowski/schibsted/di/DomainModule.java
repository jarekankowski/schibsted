package com.jarekankowski.schibsted.di;

import com.jarekankowski.schibsted.domain.BackgroundThreadSchedulerImpl;
import com.jarekankowski.schibsted.domain.MainThreadSchedulerImpl;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.UseCaseExecutorImpl;
import com.jarekankowski.schibsted.domain.subscription.CompositeSubscription;
import com.jarekankowski.schibsted.domain.subscription.CompositeSubscriptionImpl;
import com.jarekankowski.schibsted.domain.threads.BackgroundThreadScheduler;
import com.jarekankowski.schibsted.domain.threads.MainThreadScheduler;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jarekankowski on 27/08/2016.
 */
@Module(
        library = true,
        includes = {
                UseCaseModule.class
        },
        complete = false
)
public class DomainModule {

    @Provides
    @Singleton
    public UseCaseExecutor provideUseCaseExecutor(MainThreadScheduler mainThreadScheduler, BackgroundThreadScheduler backgroundScheduler, CompositeSubscription compositeSubscription) {
        return new UseCaseExecutorImpl(mainThreadScheduler, backgroundScheduler, compositeSubscription);
    }

    @Provides
    @Singleton
    public MainThreadScheduler provideMainThreadScheduler() {
        return new MainThreadSchedulerImpl();
    }

    @Provides
    @Singleton
    public BackgroundThreadScheduler provideBackgroundThreadScheduler() {
        return new BackgroundThreadSchedulerImpl();
    }

    @Provides
    @Singleton
    public CompositeSubscription provideCompositeSubscription() {
        return new CompositeSubscriptionImpl(new rx.subscriptions.CompositeSubscription());
    }
}
