package com.jarekankowski.schibsted.di;

import com.google.gson.Gson;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbListMapper;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbMapper;
import com.jarekankowski.schibsted.repository.ApiService;
import com.jarekankowski.schibsted.repository.Repository;
import com.jarekankowski.schibsted.repository.RepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

import static com.jarekankowski.schibsted.BuildConfig.API_HOST;
import static com.jarekankowski.schibsted.BuildConfig.RETROFIT_LOG;

/**
 * Created by jarekankowski on 27/08/2016.
 */
@Module(
        includes = {
                MapperModule.class
        },
        library = true
)
public class RepositoryModule {

    @Provides
    @Singleton
    public Repository provideRepository(ApiService apiService, RecipeApiDbListMapper recipeApiDbListMapper, RecipeApiDbMapper recipeApiDbMapper) {
        return new RepositoryImpl(apiService, recipeApiDbListMapper, recipeApiDbMapper);
    }

    @Provides
    @Singleton
    public ApiService provideApiRepository() {
        final RestAdapter restAdapter = buildRestAdapter();
        return restAdapter.create(ApiService.class);
    }

    private RestAdapter buildRestAdapter() {
        return new RestAdapter.Builder().setEndpoint(API_HOST)
                                        .setConverter(new GsonConverter(new Gson()))
                                        .setLogLevel(RETROFIT_LOG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                                        .build();
    }
}
