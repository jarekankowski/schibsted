package com.jarekankowski.schibsted.di;

import com.jarekankowski.schibsted.domain.interaction.LoadRecipeDetailsUseCase;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipesUseCase;
import com.jarekankowski.schibsted.repository.Repository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jarekankowski on 27/08/2016.
 */
@Module(
        complete = false,
        library = true
)
public class UseCaseModule {

    @Provides
    public LoadRecipesUseCase provideGetRecipeUseCase(Repository repository) {
        return new LoadRecipesUseCase(repository);
    }

    @Provides
    public LoadRecipeDetailsUseCase provideLoadRecipeDetailsUseCase(Repository repository) {
        return new LoadRecipeDetailsUseCase(repository);
    }
}
