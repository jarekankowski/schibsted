package com.jarekankowski.schibsted.di;

import com.jarekankowski.schibsted.data.mapper.RecipeApiDbListMapper;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jarekankowski on 27/08/2016.
 */
@Module(
        complete = false,
        library = true
)
public class MapperModule {

    @Provides
    @Singleton
    public RecipeApiDbMapper provideRecipeApiDbMapper() {
        return new RecipeApiDbMapper();
    }

    @Provides
    @Singleton
    public RecipeApiDbListMapper provideRecipeApiDbListMapper(RecipeApiDbMapper mapper) {
        return new RecipeApiDbListMapper(mapper);
    }
}
