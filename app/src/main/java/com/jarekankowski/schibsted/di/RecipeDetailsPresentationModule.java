package com.jarekankowski.schibsted.di;

import com.jarekankowski.schibsted.ui.activity.RecipeDetailsActivity;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipeDetailsUseCase;
import com.jarekankowski.schibsted.presentation.RecipeDetailsPresenter;
import com.jarekankowski.schibsted.presentation.RecipeDetailsView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jarekankowski on 29/08/2016.
 */
@Module(
        injects = RecipeDetailsActivity.class,
        complete = false
)
public class RecipeDetailsPresentationModule {

    private final RecipeDetailsView mView;

    public RecipeDetailsPresentationModule(RecipeDetailsView view) {
        mView = view;
    }

    @Provides
    public RecipeDetailsPresenter provideRecipeDetailsPresenter(UseCaseExecutor useCaseExecutor, LoadRecipeDetailsUseCase useCase) {
        return new RecipeDetailsPresenter(mView, useCaseExecutor, useCase);
    }
}
