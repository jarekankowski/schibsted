package com.jarekankowski.schibsted.di;

import android.content.Context;

import com.jarekankowski.schibsted.ui.adapter.RecipeAdapter;
import com.jarekankowski.schibsted.SchibstedApplication;
import com.jarekankowski.schibsted.utils.ImageLoader;
import com.jarekankowski.schibsted.utils.ImageLoaderImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jarekankowski on 24/08/2016.
 */
@Module(
        injects = {
                SchibstedApplication.class,
                RecipeAdapter.class
        },
        includes = {
                DomainModule.class,
                RepositoryModule.class
        },
        library = true

)
public class ApplicationModule {

    private final Context mContext;

    public ApplicationModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    public ImageLoader provideImageLoader() {
        return new ImageLoaderImpl(mContext);
    }
}
