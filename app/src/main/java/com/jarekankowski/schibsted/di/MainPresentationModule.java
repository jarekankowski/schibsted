package com.jarekankowski.schibsted.di;

import com.jarekankowski.schibsted.ui.fragment.MainFragment;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipesUseCase;
import com.jarekankowski.schibsted.presentation.RecipePresenter;
import com.jarekankowski.schibsted.presentation.RecipeView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by jarekankowski on 24/08/2016.
 */
@Module(
        injects = MainFragment.class,
        complete = false
)
public class MainPresentationModule {

    private final RecipeView mView;

    public MainPresentationModule(RecipeView view) {
        mView = view;
    }

    @Provides
    public RecipePresenter provideMainPresenter(UseCaseExecutor interactionExecutor, LoadRecipesUseCase loadRecipesUseCase) {
        return new RecipePresenter(mView, interactionExecutor, loadRecipesUseCase);
    }
}
