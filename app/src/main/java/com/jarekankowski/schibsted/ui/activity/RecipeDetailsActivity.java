package com.jarekankowski.schibsted.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.jarekankowski.schibsted.R;
import com.jarekankowski.schibsted.SchibstedApplication;
import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.di.RecipeDetailsPresentationModule;
import com.jarekankowski.schibsted.presentation.RecipeDetailsPresenter;
import com.jarekankowski.schibsted.presentation.RecipeDetailsView;
import com.jarekankowski.schibsted.ui.adapter.RecipeIngredientsAdapter;
import com.jarekankowski.schibsted.utils.ImageLoader;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import icepick.Icepick;
import icepick.State;

/**
 * Created by jarekankowski on 30/08/2016.
 */
public class RecipeDetailsActivity
        extends BaseToolbarActivity
        implements RecipeDetailsView {

    private static final String KEY_RECIPE = "KEY_RECIPE";

    // region views
    @BindView(R.id.activity_recipe_details_image)
    ImageView               mImageView;

    @BindView(R.id.activity_recipe_details_recycler_view)
    RecyclerView            mRecyclerView;

    @BindView(R.id.activity_recipe_details_publisher_name)
    TextView                mPublisherNameTextView;

    @BindView(R.id.activity_recipe_details_social_rank)
    TextView                mSocialRankTextView;

    @BindView(R.id.loading_view)
    View                    mLoadingView;

    @BindView(R.id.activity_recipe_details_ingredients_container)
    View                    mIngredientsContainerView;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    // endregion

    // region data
    @Inject
    RecipeDetailsPresenter  mPresenter;

    @Inject
    ImageLoader             mImageLoader;

    @State
    Recipe                  mRecipe;

    private RecipeIngredientsAdapter mIngredientsAdapter;
    // endregion

    public static void launch(Activity activity, Recipe recipe) {
        final Intent intent = new Intent(activity, RecipeDetailsActivity.class);
        intent.putExtra(KEY_RECIPE, recipe);
        activity.startActivity(intent);
    }

    // region lifecycle methods
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((SchibstedApplication) getApplication()).extendScopedGraph(new RecipeDetailsPresentationModule(this))
                                                 .inject(this);

        if (savedInstanceState == null) {
            mRecipe = (Recipe) getIntent().getSerializableExtra(KEY_RECIPE);
            mPresenter.onCreate(mRecipe.getId());
        } else {
            Icepick.restoreInstanceState(this, savedInstanceState);
        }

        initAdapter();
        populateUI();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }
    // endregion

    //region inherited methods
    @Override
    protected int getLayoutId() {
        return R.layout.activity_recipe_details;
    }

    @OnClick(R.id.activity_recipe_details_instructions)
    public void onViewInstructionsClicked() {
        WebViewActivity.launch(this, mRecipe.getInstructionsUrl());
    }

    @OnClick(R.id.activity_recipe_details_original)
    public void onViewOriginalClicked() {
        WebViewActivity.launch(this, mRecipe.getOriginalUrl());
    }

    @Override
    public void showLoader() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideIngredientsView() {
        mIngredientsContainerView.setVisibility(View.GONE);
    }

    @Override
    public void hideLoadingView() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void updateRecipeDetails(Recipe recipe) {
        mRecipe = recipe;
        populateUI();
    }
    // endregion

    private void initAdapter() {
        mIngredientsAdapter = new RecipeIngredientsAdapter();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mIngredientsAdapter);
    }

    private void populateUI() {
        mCollapsingToolbarLayout.setTitle(mRecipe.getName());
        mImageLoader.loadRoundedSquareImage(mRecipe.getUrl(), mImageView);
        mPublisherNameTextView.setText(mRecipe.getPublisherName());
        mSocialRankTextView.setText(buildRankString());
        mIngredientsAdapter.setIngredients(mRecipe.getIngredients());
    }

    private String buildRankString() {
        final int round = Math.round(Float.valueOf(mRecipe.getSocialRank()));
        return getString(R.string.activity_recipe_details_social_rank) + " " + round;
    }
}
