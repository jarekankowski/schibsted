package com.jarekankowski.schibsted.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jarekankowski.schibsted.R;
import com.jarekankowski.schibsted.ui.fragment.MainFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class MainActivity
        extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        if (savedInstanceState == null) {
            showFragment();
        }
    }

    private void showFragment() {
        getSupportFragmentManager().beginTransaction()
                                   .replace(R.id.activity_main_content, MainFragment.newInstance())
                                   .commit();
    }
}
