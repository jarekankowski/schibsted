package com.jarekankowski.schibsted.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jarekankowski.schibsted.R;

import butterknife.BindView;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public class WebViewActivity
        extends BaseToolbarActivity {

    private static final String KEY_URL = "key_url";

    @BindView(R.id.activity_webview_webview)
    WebView mWebView;

    public static void launch(Activity activity, String url) {
        final Intent intent = new Intent(activity, WebViewActivity.class);
        intent.putExtra(KEY_URL, url);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWebView();
    }

    private void initWebView() {
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl(getIntent().getStringExtra(KEY_URL));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_webview;
    }
}
