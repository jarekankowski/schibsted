package com.jarekankowski.schibsted.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.jarekankowski.schibsted.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jarekankowski on 30/08/2016.
 */
public abstract class BaseToolbarActivity
        extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar mToolbarView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);

        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(mToolbarView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    protected abstract int getLayoutId();
}
