package com.jarekankowski.schibsted.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jarekankowski.schibsted.R;
import com.jarekankowski.schibsted.SchibstedApplication;
import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.utils.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jarekankowski on 30/08/2016.
 */
public class RecipeAdapter
        extends RecyclerView.Adapter<RecipeAdapter.RecipeViewHolder> {

    @Inject
    ImageLoader mImageLoader;

    private final ItemClickedCallback mItemClickedCallback;

    private       List<Recipe>        mRecipeList;

    public RecipeAdapter(Context context, ItemClickedCallback itemClickedCallback) {
        mItemClickedCallback = itemClickedCallback;
        mRecipeList = new ArrayList<>();
        ((SchibstedApplication) context.getApplicationContext()).extendScopedGraph()
                                                                .inject(this);
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new RecipeViewHolder(layoutInflater.inflate(R.layout.item_recipe_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {
        holder.populate(mRecipeList.get(position));
    }

    @Override
    public int getItemCount() {
        return mRecipeList.size();
    }

    public void showRecipes(List<Recipe> recipes) {
        mRecipeList = new ArrayList<>(recipes);
        notifyDataSetChanged();
    }

    class RecipeViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.item_recipe_image)
        ImageView mRecipeImageView;

        @BindView(R.id.item_recipe_name)
        TextView  mRecipeTextView;

        RecipeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void populate(Recipe recipe) {
            mRecipeTextView.setText(recipe.getName());
            mImageLoader.loadRoundedSquareImage(recipe.getUrl(), mRecipeImageView);
        }

        @OnClick(R.id.item_recipe_container)
        public void onItemClicked() {
            final Recipe recipe = mRecipeList.get(getAdapterPosition());
            mItemClickedCallback.onItemClicked(recipe);
        }
    }

    public interface ItemClickedCallback {

        void onItemClicked(Recipe recipe);
    }
}
