package com.jarekankowski.schibsted.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jarekankowski.schibsted.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jarekankowski on 30/08/2016.
 */
public class RecipeIngredientsAdapter
        extends RecyclerView.Adapter<RecipeIngredientsAdapter.RecipeIngredientsViewHolder> {

    private List<String> mIngredients = new ArrayList<>();

    @Override
    public RecipeIngredientsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                                        .inflate(R.layout.item_ingredient, parent, false);
        return new RecipeIngredientsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipeIngredientsViewHolder holder, int position) {
        holder.populate(mIngredients.get(position));
    }

    @Override
    public int getItemCount() {
        return mIngredients.size();
    }

    public void setIngredients(List<String> ingredients) {
        mIngredients = new ArrayList<>(ingredients);
        notifyDataSetChanged();
    }

    class RecipeIngredientsViewHolder
            extends RecyclerView.ViewHolder {

        @BindView(R.id.item_ingredient_name)
        TextView mTextView;

        RecipeIngredientsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void populate(String ingredientName) {
            mTextView.setText(ingredientName);
        }
    }
}
