package com.jarekankowski.schibsted.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.rxbinding.support.v7.widget.RxSearchView;
import com.jarekankowski.schibsted.R;
import com.jarekankowski.schibsted.SchibstedApplication;
import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.di.MainPresentationModule;
import com.jarekankowski.schibsted.presentation.RecipePresenter;
import com.jarekankowski.schibsted.presentation.RecipeView;
import com.jarekankowski.schibsted.ui.activity.RecipeDetailsActivity;
import com.jarekankowski.schibsted.ui.adapter.RecipeAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import icepick.Icepick;
import icepick.State;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

import static android.support.v4.view.MenuItemCompat.getActionView;

/**
 * Created by jarekankowski on 28/08/2016.
 */
public class MainFragment
        extends Fragment
        implements RecipeView, RecipeAdapter.ItemClickedCallback {

    private static final int QUERY_DELAY = 400;

    // region views
    @BindView(R.id.fragment_main_recyclerview)
    RecyclerView      mRecyclerView;

    @BindView(R.id.loading_view)
    View              mLoadingView;

    @BindView(R.id.fragment_main_placeholder)
    View              mPlaceholderView;

    @BindView(R.id.fragment_main_intro)
    View              mIntroView;
    // endregion

    // region data
    @Inject
    RecipePresenter   mPresenter;

    @State
    ArrayList<Recipe> mRecipes;

    @State
    String            mQuery;

    private Unbinder              mUnbinder;

    private RecipeAdapter         mRecipeAdapter;

    private CompositeSubscription mCompositeSubscription;
    // endregion

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    // region lifecycle methods
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((SchibstedApplication) context.getApplicationContext()).extendScopedGraph(new MainPresentationModule(this))
                                                                .inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mCompositeSubscription = new CompositeSubscription();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        mRecipeAdapter = new RecipeAdapter(getContext(), this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mRecipeAdapter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        if (StringUtils.isNotBlank(mQuery)) {
            mPresenter.onNext(mRecipes);
        } else {
            mIntroView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Icepick.saveInstanceState(this, outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        mCompositeSubscription.unsubscribe();
        mUnbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        mPresenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        final MenuItem searchMenuItem = menu.findItem(R.id.actionSearch);
        final SearchView searchView = (SearchView) getActionView(searchMenuItem);

        searchView.findViewById(R.id.search_close_btn)
                  .setOnClickListener(v -> mPresenter.onSearchQueryFilled(mQuery));
        final Subscription subscribe = buildSearchVIewSubscription(searchView);
        mCompositeSubscription.add(subscribe);
    }

    private Subscription buildSearchVIewSubscription(SearchView searchView) {
        return RxSearchView.queryTextChanges(searchView)
                           .debounce(QUERY_DELAY, TimeUnit.MILLISECONDS)
                           .observeOn(AndroidSchedulers.mainThread())
                           .map(CharSequence::toString)
                           .filter(StringUtils::isNotBlank)
                           .subscribe(query -> {
                               mQuery = query;
                               mPresenter.onSearchQueryFilled(mQuery);
                           });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.actionSearch) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    // endregion

    //region inherited methods
    @Override
    public void showError() {
        if (getView() != null) {
            showSnackbar();
        }
    }

    private void showSnackbar() {
        Snackbar.make(getView(), R.string.error_loading_data, Snackbar.LENGTH_LONG)
                .setAction(R.string.retry, view -> mPresenter.onSearchQueryFilled(mQuery))
                .show();
    }

    @Override
    public void showRecipes(List<Recipe> recipes) {
        mRecipes = new ArrayList<>(recipes);
        mRecipeAdapter.showRecipes(mRecipes);
    }

    @Override
    public void showLoader() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePlaceholder() {
        mPlaceholderView.setVisibility(View.GONE);
    }

    @Override
    public void hideList() {
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideIntro() {
        mIntroView.setVisibility(View.GONE);
    }

    @Override
    public void showList() {
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        mLoadingView.setVisibility(View.GONE);
    }

    @Override
    public void showPlaceholder() {
        mPlaceholderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClicked(Recipe recipe) {
        RecipeDetailsActivity.launch(getActivity(), recipe);
    }

    // endregion
}
