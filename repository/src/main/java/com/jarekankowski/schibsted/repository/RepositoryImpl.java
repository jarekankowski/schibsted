package com.jarekankowski.schibsted.repository;

import com.jarekankowski.schibsted.data.api.model.ApiRecipeDetails;
import com.jarekankowski.schibsted.data.api.model.ApiRecipeList;
import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbListMapper;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbMapper;
import com.jarekankowski.schibsted.repository.exceptions.ApiException;

import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class RepositoryImpl
        implements Repository {

    private static final String KEY = "b549c4c96152e677eb90de4604ca61a2";

    private final ApiService            mApiService;

    private final RecipeApiDbListMapper mRecipeApiDbListMapper;

    private final RecipeApiDbMapper     mRecipeApiDbMapper;

    public RepositoryImpl(ApiService apiService, RecipeApiDbListMapper recipeApiDbListMapper, RecipeApiDbMapper recipeApiDbMapper) {
        mApiService = apiService;
        mRecipeApiDbListMapper = recipeApiDbListMapper;
        mRecipeApiDbMapper = recipeApiDbMapper;
    }

    @Override
    public List<Recipe> loadRecipes(String query)
            throws ApiException {
        try {
            final ApiRecipeList apiRecipeList = mApiService.getRecipeList(KEY, query, null, null);
            return mRecipeApiDbListMapper.lowerToUpperLayer(apiRecipeList);
        } catch (Throwable t) {
            throw new ApiException(t);
        }
    }

    @Override
    public Recipe loadRecipeDetails(String recipeId)
            throws ApiException {
        try {
            final ApiRecipeDetails apiRecipeDetails = mApiService.getRecipeDetails(KEY, recipeId);
            return mRecipeApiDbMapper.lowerToUpperLayer(apiRecipeDetails.getRecipe());
        } catch (Throwable t) {
            throw new ApiException(t);
        }
    }
}
