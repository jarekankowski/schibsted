package com.jarekankowski.schibsted.repository;

import com.jarekankowski.schibsted.data.api.model.ApiRecipeDetails;
import com.jarekankowski.schibsted.data.api.model.ApiRecipeList;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public interface ApiService {

    @GET("/get")
    ApiRecipeDetails getRecipeDetails(@Query("key") String key, @Query("rId") String recipeId);

    @GET("/search")
    ApiRecipeList getRecipeList(@Query("key") String key, @Query("q") String query, @Query("sort") String sortType, @Query("page") Integer pageNumber);
}
