package com.jarekankowski.schibsted.repository;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.repository.exceptions.ApiException;

import java.util.List;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public interface Repository {

    List<Recipe> loadRecipes(String query)
            throws ApiException;

    Recipe loadRecipeDetails(String recipeId)
            throws ApiException;
}
