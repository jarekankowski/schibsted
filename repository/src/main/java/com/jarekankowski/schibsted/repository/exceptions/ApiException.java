package com.jarekankowski.schibsted.repository.exceptions;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class ApiException
        extends Exception {

    public ApiException(Throwable t) {
        super(t);
    }
}
