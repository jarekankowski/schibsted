package com.jarekankowski.schibsted.repository;

import com.jarekankowski.schibsted.data.api.model.ApiRecipe;
import com.jarekankowski.schibsted.data.api.model.ApiRecipeDetails;
import com.jarekankowski.schibsted.data.api.model.ApiRecipeList;
import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbListMapper;
import com.jarekankowski.schibsted.data.mapper.RecipeApiDbMapper;
import com.jarekankowski.schibsted.repository.exceptions.ApiException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class RepositoryImplTest {

    private static final String KEY       = "b549c4c96152e677eb90de4604ca61a2";

    private static final String QUERY     = "query";

    private static final String RECIPE_ID = "recipeId";

    private Repository            mRepository;

    @Mock
    private ApiService            mApiService;

    @Mock
    private RecipeApiDbListMapper mRecipeApiDbListMapper;

    @Mock
    private RecipeApiDbMapper     mRecipeApiDbMapper;

    @Mock
    private ApiRecipeList         mApiRecipeList;

    @Mock
    private ApiRecipe             mApiRecipe;

    @Mock
    private Recipe                mRecipe;

    private List<Recipe>          mRecipeList;

    @Mock
    private ApiRecipeDetails      mApiRecipeDetails;

    @Mock
    private RecipeApiDbMapper     RecipeApiDbMapper;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);
        initMocks();
        mRepository = new RepositoryImpl(mApiService, mRecipeApiDbListMapper, mRecipeApiDbMapper);
    }

    @Test(expected = ApiException.class)
    public void testOnLoadRecipes_WhenRetrofitError_ShouldThrowApiException()
            throws Exception {
        when(mApiService.getRecipeList(anyString(), anyString(), anyString(), anyInt())).thenThrow(new RuntimeException());

        mRepository.loadRecipes(QUERY);
    }

    @Test(expected = ApiException.class)
    public void testOnLoadRecipes_WhenMapperError_ShouldThrowApiException()
            throws Exception {
        when(mApiService.getRecipeList(anyString(), anyString(), anyString(), anyInt())).thenReturn(mApiRecipeList);
        when(mRecipeApiDbListMapper.lowerToUpperLayer(any(ApiRecipeList.class))).thenThrow(new RuntimeException());

        mRepository.loadRecipes(QUERY);
    }

    @Test
    public void testOnLoadRecipes()
            throws Exception {

        when(mApiService.getRecipeList(KEY, QUERY, null, null)).thenReturn(mApiRecipeList);
        when(mRecipeApiDbListMapper.lowerToUpperLayer(mApiRecipeList)).thenReturn(mRecipeList);

        final List<Recipe> recipes = mRepository.loadRecipes(QUERY);

        final Recipe recipe = recipes.get(0);
        assertEquals(recipe.getId(), mApiRecipe.getRecipeId());
    }

    @Test(expected = ApiException.class)
    public void testOnLoadRecipeDetails_WhenRetrofitError_ShouldThrowApiException()
            throws Exception {
        when(mApiService.getRecipeDetails(anyString(), anyString())).thenThrow(new RuntimeException());

        mRepository.loadRecipeDetails(QUERY);
    }

    @Test(expected = ApiException.class)
    public void testOnLoadRecipeDetails_WhenMapperError_ShouldThrowApiException()
            throws Exception {
        when(mApiService.getRecipeDetails(anyString(), anyString())).thenReturn(mApiRecipeDetails);
        when(mRecipeApiDbMapper.lowerToUpperLayer(any(ApiRecipe.class))).thenThrow(new RuntimeException());

        mRepository.loadRecipeDetails(QUERY);
    }

    @Test
    public void testOnLoadRecipeDetails()
            throws Exception {

        when(mApiService.getRecipeDetails(KEY, QUERY)).thenReturn(mApiRecipeDetails);
        when(mRecipeApiDbMapper.lowerToUpperLayer(mApiRecipe)).thenReturn(mRecipe);

        final Recipe recipe = mRepository.loadRecipeDetails(QUERY);
        assertEquals(recipe.getId(), mApiRecipe.getRecipeId());
    }

    private void initMocks() {
        when(mApiRecipe.getRecipeId()).thenReturn(RECIPE_ID);

        final List<ApiRecipe> apiRecipeList = new ArrayList<>();
        apiRecipeList.add(mApiRecipe);
        when(mApiRecipeList.getApiRecipeList()).thenReturn(apiRecipeList);

        when(mRecipe.getId()).thenReturn(RECIPE_ID);
        mRecipeList = new ArrayList<>();
        mRecipeList.add(mRecipe);

        when(mApiRecipeDetails.getRecipe()).thenReturn(mApiRecipe);
    }
}