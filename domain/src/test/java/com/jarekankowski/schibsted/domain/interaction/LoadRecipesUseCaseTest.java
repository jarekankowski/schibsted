package com.jarekankowski.schibsted.domain.interaction;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.repository.Repository;
import com.jarekankowski.schibsted.repository.exceptions.ApiException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import rx.Observable;
import rx.exceptions.OnErrorThrowable;
import rx.observers.TestSubscriber;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class LoadRecipesUseCaseTest {

    private static final String QUERY = "query";

    @Mock
    private Repository         mRepository;

    private LoadRecipesUseCase mLoadRecipesUseCase;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);

        mLoadRecipesUseCase = new LoadRecipesUseCase(mRepository);
        mLoadRecipesUseCase.setSearchQuery(QUERY);
    }

    @Test
    public void testOnBuildObservable()
            throws Exception {
        executeAndSubscribe();

        verify(mRepository).loadRecipes(QUERY);
        verifyNoMoreInteractions(mRepository);
    }

    @Test
    public void testOnBuildObservable_WhenError()
            throws Exception {
        when(mRepository.loadRecipes(anyString())).thenThrow(new ApiException(new Throwable()));

        final TestSubscriber<List<Recipe>> recipeTestSubscriber = executeAndSubscribe();
        recipeTestSubscriber.assertError(OnErrorThrowable.class);
    }

    private TestSubscriber<List<Recipe>> executeAndSubscribe() {
        final TestSubscriber<List<Recipe>> subscriber = new TestSubscriber<>();
        final Observable<List<Recipe>> finishedEventObservable = mLoadRecipesUseCase.buildObservable();
        finishedEventObservable.subscribe(subscriber);
        return subscriber;
    }
}