package com.jarekankowski.schibsted.domain.interaction;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.repository.Repository;
import com.jarekankowski.schibsted.repository.exceptions.ApiException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Observable;
import rx.exceptions.OnErrorThrowable;
import rx.observers.TestSubscriber;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Created by jarekankowski on 28/08/2016.
 */
public class LoadRecipeDetailsUseCaseTest {

    private static final String RECIPE_ID = "recipeId";

    @Mock
    private Repository               mRepository;

    private LoadRecipeDetailsUseCase mLoadRecipeDetailsUseCase;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);

        mLoadRecipeDetailsUseCase = new LoadRecipeDetailsUseCase(mRepository);
        mLoadRecipeDetailsUseCase.setRecipeId(RECIPE_ID);
    }

    @Test
    public void testOnBuildObservable()
            throws Exception {
        executeAndSubscribe();

        verify(mRepository).loadRecipeDetails(RECIPE_ID);
        verifyNoMoreInteractions(mRepository);
    }

    @Test
    public void testOnBuildObservable_WhenError()
            throws Exception {
        when(mRepository.loadRecipeDetails(anyString())).thenThrow(new ApiException(new Throwable()));

        final TestSubscriber<Recipe> recipeTestSubscriber = executeAndSubscribe();
        recipeTestSubscriber.assertError(OnErrorThrowable.class);
    }

    private TestSubscriber<Recipe> executeAndSubscribe() {
        final TestSubscriber<Recipe> subscriber = new TestSubscriber<>();
        final Observable<Recipe> finishedEventObservable = mLoadRecipeDetailsUseCase.buildObservable();
        finishedEventObservable.subscribe(subscriber);
        return subscriber;
    }
}