package com.jarekankowski.schibsted.domain;

import com.jarekankowski.schibsted.domain.interaction.UseCase;
import com.jarekankowski.schibsted.domain.subscription.CompositeSubscription;
import com.jarekankowski.schibsted.domain.threads.BackgroundThreadScheduler;
import com.jarekankowski.schibsted.domain.threads.MainThreadScheduler;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class UseCaseExecutorImplTest {

    private static final String RX_SCHEDULER_IO         = "RxIoScheduler";

    private static final String RX_SCHEDULER_NEW_THREAD = "RxNewThreadScheduler";

    private UseCaseExecutor           mUseCaseExecutor;

    private TestSubscriber<Object>    mTestSubscriber;

    @Mock
    private MainThreadScheduler       mMainThreadScheduler;

    @Mock
    private BackgroundThreadScheduler mBackgroundThreadScheduler;

    @Mock
    private UseCase                   mUseCase;

    @Mock
    private Subscription              mSubscription;

    @Mock
    private CompositeSubscription     mCompositeSubscription;

    @Before
    public void setUp()
            throws Exception {
        initMocks(this);

        mUseCaseExecutor = new UseCaseExecutorImpl(mMainThreadScheduler, mBackgroundThreadScheduler, mCompositeSubscription);

        mTestSubscriber = TestSubscriber.create();
    }

    @Test
    public void testOnSubscribe_SubscriberIsRunningOnBackgroundThread_AndObserverIsRunningOnNewThread()
            throws Exception {

        final Observable<String> observable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                subscriber.onNext(Thread.currentThread()
                                        .getName());
                subscriber.onCompleted();
            }
        });

        when(mBackgroundThreadScheduler.getScheduler()).thenReturn(Schedulers.io());
        when(mMainThreadScheduler.getScheduler()).thenReturn(Schedulers.newThread());

        when(mUseCase.buildObservable()).thenReturn(observable);

        mUseCaseExecutor.subscribe(mUseCase, mTestSubscriber);

        mTestSubscriber.awaitTerminalEvent();
        mTestSubscriber.assertNoErrors();

        verifySubscriberRunsOnBackgroundThread();

        verifyObserverRunsOnNewThread();

        verify(mCompositeSubscription).add(any(Subscription.class));
        verifyNoMoreInteractions(mCompositeSubscription);
    }

    @Test
    public void testOnUnsubscribe()
            throws Exception {
        mUseCaseExecutor.unsubscribe();

        verify(mCompositeSubscription).unsubscribe();
        verifyNoMoreInteractions(mCompositeSubscription);
    }

    private void verifySubscriberRunsOnBackgroundThread() {
        final String nextEventResult = (String) mTestSubscriber.getOnNextEvents()
                                                               .get(0);
        assertTrue(nextEventResult.startsWith(RX_SCHEDULER_IO));
    }

    private void verifyObserverRunsOnNewThread() {
        final String name = mTestSubscriber.getLastSeenThread()
                                           .getName();
        assertTrue(name.startsWith(RX_SCHEDULER_NEW_THREAD));
    }
}