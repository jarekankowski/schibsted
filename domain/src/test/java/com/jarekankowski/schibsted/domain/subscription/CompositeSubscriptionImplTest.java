package com.jarekankowski.schibsted.domain.subscription;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class CompositeSubscriptionImplTest {

    private CompositeSubscriptionImpl mCompositeSubscription;

    @Mock
    private Subscription              mSubscription;

    private CompositeSubscription     mRXCompositeSubscription;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);

        mRXCompositeSubscription = new CompositeSubscription();
        mCompositeSubscription = new CompositeSubscriptionImpl(mRXCompositeSubscription);
    }

    @Test
    public void testOnAdd()
            throws Exception {
        mCompositeSubscription.add(mSubscription);

        assertTrue(mRXCompositeSubscription.hasSubscriptions());
    }

    @Test
    public void testOnUnsubscribe()
            throws Exception {
        mCompositeSubscription.unsubscribe();

        assertFalse(mRXCompositeSubscription.hasSubscriptions());
    }
}