package com.jarekankowski.schibsted.domain.interaction;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.repository.Repository;

import rx.Observable;
import rx.exceptions.OnErrorThrowable;

/**
 * Created by jarekankowski on 28/08/2016.
 */
public class LoadRecipeDetailsUseCase
        implements UseCase<Recipe> {

    private final Repository mRepository;

    private       String     mRecipeId;

    public LoadRecipeDetailsUseCase(Repository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<Recipe> buildObservable() {
        return Observable.defer(() -> {
            try {
                return Observable.just(mRepository.loadRecipeDetails(mRecipeId));
            } catch (Throwable e) {
                throw OnErrorThrowable.from(e);
            }
        });
    }

    public void setRecipeId(String recipeId) {
        mRecipeId = recipeId;
    }
}
