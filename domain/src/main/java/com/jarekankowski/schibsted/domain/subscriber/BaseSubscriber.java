package com.jarekankowski.schibsted.domain.subscriber;

import rx.Observer;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public abstract class BaseSubscriber<ResultType>
        implements Observer<ResultType> {

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
    }

    @Override
    public void onNext(ResultType t) {

    }
}
