package com.jarekankowski.schibsted.domain.interaction;

import rx.Observable;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public interface UseCase<ResultType> {

    Observable<ResultType> buildObservable();
}
