package com.jarekankowski.schibsted.domain.interaction;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.repository.Repository;

import java.util.List;

import rx.Observable;
import rx.exceptions.OnErrorThrowable;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class LoadRecipesUseCase
        implements UseCase<List<Recipe>> {

    private final Repository mRepository;

    private       String     mSearchQuery;

    public LoadRecipesUseCase(Repository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<List<Recipe>> buildObservable() {
        return Observable.defer(() -> {
            try {
                return Observable.just(mRepository.loadRecipes(mSearchQuery));
            } catch (Throwable e) {
                throw OnErrorThrowable.from(e);
            }
        });
    }

    public void setSearchQuery(String searchQuery) {
        mSearchQuery = searchQuery;
    }
}
