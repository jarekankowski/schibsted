package com.jarekankowski.schibsted.domain;

import com.jarekankowski.schibsted.domain.interaction.UseCase;
import com.jarekankowski.schibsted.domain.subscription.CompositeSubscription;
import com.jarekankowski.schibsted.domain.threads.BackgroundThreadScheduler;
import com.jarekankowski.schibsted.domain.threads.MainThreadScheduler;

import rx.Observer;
import rx.Subscription;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class UseCaseExecutorImpl
        implements UseCaseExecutor {

    private final MainThreadScheduler       mMainThreadScheduler;

    private final BackgroundThreadScheduler mBackgroundThreadScheduler;

    private final CompositeSubscription     mCompositeSubscription;

    public UseCaseExecutorImpl(MainThreadScheduler mainThreadScheduler, BackgroundThreadScheduler backgroundThreadScheduler, CompositeSubscription compositeSubscription) {
        mMainThreadScheduler = mainThreadScheduler;
        mBackgroundThreadScheduler = backgroundThreadScheduler;
        mCompositeSubscription = compositeSubscription;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void subscribe(UseCase useCase, Observer observer) {
        final Subscription subscribe = useCase.buildObservable()
                                              .subscribeOn(mBackgroundThreadScheduler.getScheduler())
                                              .observeOn(mMainThreadScheduler.getScheduler())
                                              .subscribe(observer);
        mCompositeSubscription.add(subscribe);
    }

    @Override
    public void unsubscribe() {
        mCompositeSubscription.unsubscribe();
    }
}
