package com.jarekankowski.schibsted.domain;

import com.jarekankowski.schibsted.domain.interaction.UseCase;

import rx.Observer;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public interface UseCaseExecutor {

    void subscribe(UseCase useCase, Observer observer);

    void unsubscribe();
}
