package com.jarekankowski.schibsted.domain.threads;

import rx.Scheduler;

/**
 * Created by jarekankowski on 15/07/2016.
 */
public interface ThreadScheduler {

    Scheduler getScheduler();
}
