package com.jarekankowski.schibsted.domain.subscription;

import rx.Subscription;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public class CompositeSubscriptionImpl
        implements CompositeSubscription {

    private final rx.subscriptions.CompositeSubscription mCompositeSubscription;

    public CompositeSubscriptionImpl(rx.subscriptions.CompositeSubscription compositeSubscription) {
        mCompositeSubscription = compositeSubscription;
    }

    @Override
    public void add(Subscription subscription) {
        mCompositeSubscription.add(subscription);
    }

    @Override
    public void unsubscribe() {
        mCompositeSubscription.unsubscribe();
    }
}