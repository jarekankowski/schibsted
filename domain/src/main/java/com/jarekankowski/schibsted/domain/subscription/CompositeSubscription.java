package com.jarekankowski.schibsted.domain.subscription;

import rx.Subscription;

/**
 * Created by jarekankowski on 27/08/2016.
 */
public interface CompositeSubscription {

    void add(final Subscription subscription);

    void unsubscribe();
}
