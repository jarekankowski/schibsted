package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipeDetailsUseCase;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public class RecipeDetailsPresenter
        extends BasePresenter<Recipe> {

    private final RecipeDetailsView        mView;

    private final LoadRecipeDetailsUseCase mLoadRecipeDetailsUseCase;

    public RecipeDetailsPresenter(RecipeDetailsView view, UseCaseExecutor useCaseExecutor, LoadRecipeDetailsUseCase loadRecipeDetailsUseCase) {
        super(useCaseExecutor);
        mView = view;
        mLoadRecipeDetailsUseCase = loadRecipeDetailsUseCase;
    }

    public void onCreate(String recipeId) {
        mView.showLoader();
        mLoadRecipeDetailsUseCase.setRecipeId(recipeId);
        subscribe(mLoadRecipeDetailsUseCase);
    }

    @Override
    public void onError(Throwable e) {
        mView.hideLoadingView();
        mView.hideIngredientsView();
    }

    @Override
    public void onNext(Recipe recipe) {
        mView.hideLoadingView();
        mView.updateRecipeDetails(recipe);
    }
}
