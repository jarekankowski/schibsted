package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.data.db.model.Recipe;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public interface RecipeDetailsView {

    void showLoader();

    void hideIngredientsView();

    void hideLoadingView();

    void updateRecipeDetails(Recipe recipe);
}
