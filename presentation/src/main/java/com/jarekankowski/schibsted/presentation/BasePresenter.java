package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.UseCase;
import com.jarekankowski.schibsted.domain.subscriber.BaseSubscriber;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public abstract class BasePresenter<T>
        extends BaseSubscriber<T> {

    private final UseCaseExecutor mUseCaseExecutor;

    BasePresenter(UseCaseExecutor useCaseExecutor) {
        mUseCaseExecutor = useCaseExecutor;
    }

    public void onDestroy() {
        mUseCaseExecutor.unsubscribe();
    }

    protected void subscribe(UseCase useCase) {
        mUseCaseExecutor.subscribe(useCase, this);
    }
}
