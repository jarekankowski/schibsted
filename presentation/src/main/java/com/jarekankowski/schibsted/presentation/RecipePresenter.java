package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipesUseCase;

import org.apache.commons.collections4.CollectionUtils;

import java.util.List;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public class RecipePresenter
        extends BasePresenter<List<Recipe>> {

    private final RecipeView         mView;

    private final LoadRecipesUseCase mLoadRecipesUseCase;

    public RecipePresenter(RecipeView view, UseCaseExecutor useCaseExecutor, LoadRecipesUseCase loadRecipesUseCase) {
        super(useCaseExecutor);
        mView = view;
        mLoadRecipesUseCase = loadRecipesUseCase;
    }

    public void onSearchQueryFilled(String query) {
        mView.showLoader();
        mView.hideList();
        mView.hidePlaceholder();
        mView.hideIntro();
        mLoadRecipesUseCase.setSearchQuery(query);
        subscribe(mLoadRecipesUseCase);
    }

    @Override
    public void onError(Throwable e) {
        mView.hideLoader();
        mView.showError();
    }

    @Override
    public void onNext(List<Recipe> recipes) {
        mView.hideLoader();
        if (CollectionUtils.isNotEmpty(recipes)) {
            mView.hidePlaceholder();
            mView.showList();
            mView.showRecipes(recipes);
        } else {
            mView.showPlaceholder();
            mView.hideList();
        }
    }
}
