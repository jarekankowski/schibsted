package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.data.db.model.Recipe;

import java.util.List;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public interface RecipeView {

    void showError();

    void showRecipes(List<Recipe> recipes);

    void hideLoader();

    void showPlaceholder();

    void hidePlaceholder();

    void hideList();

    void showLoader();

    void hideIntro();

    void showList();
}
