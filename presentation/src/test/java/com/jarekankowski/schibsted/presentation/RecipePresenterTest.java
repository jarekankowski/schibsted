package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipesUseCase;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public class RecipePresenterTest {

    private static final String QUERY = "query";

    @Mock
    private RecipeView         mView;

    @Mock
    private UseCaseExecutor    mUseCaseExecutor;

    @Mock
    private LoadRecipesUseCase mLoadRecipesUseCase;

    private RecipePresenter    mRecipePresenter;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);
        mRecipePresenter = new RecipePresenter(mView, mUseCaseExecutor, mLoadRecipesUseCase);
    }

    @Test
    public void testOnOnSearchQueryFilled()
            throws Exception {
        mRecipePresenter.onSearchQueryFilled(QUERY);

        verify(mView).showLoader();
        verify(mView).hideList();
        verify(mView).hidePlaceholder();
        verify(mView).hideIntro();

        verify(mLoadRecipesUseCase).setSearchQuery(QUERY);

        verify(mUseCaseExecutor).subscribe(argThat(is(mLoadRecipesUseCase)), argThat(is(mRecipePresenter)));

        verifyNoMoreInteractions(mView);
        verifyNoMoreInteractions(mLoadRecipesUseCase);
        verifyNoMoreInteractions(mUseCaseExecutor);
    }

    @Test
    public void testOnOnError()
            throws Exception {
        mRecipePresenter.onError(new Exception());

        verify(mView).hideLoader();
        verify(mView).showError();

        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testOnOnNext_WhenListIsEmpty()
            throws Exception {
        mRecipePresenter.onNext(Collections.emptyList());

        verify(mView).hideLoader();
        verify(mView).showPlaceholder();
        verify(mView).hideList();

        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testOnOnNext_WhenListIsNotEmpty()
            throws Exception {
        final ArrayList<Recipe> recipes = new ArrayList<>();
        recipes.add(Mockito.mock(Recipe.class));

        mRecipePresenter.onNext(recipes);

        verify(mView).hideLoader();
        verify(mView).hidePlaceholder();
        verify(mView).showList();
        verify(mView).showRecipes(argThat(is(recipeListMatcher(recipes))));

        verifyNoMoreInteractions(mView);
    }

    private Matcher<List<Recipe>> recipeListMatcher(List<Recipe> recipes) {
        return new BaseMatcher<List<Recipe>>() {
            @Override
            public boolean matches(Object item) {
                final List<Recipe> list = (List<Recipe>) item;
                return list.size() == recipes.size();
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }
}