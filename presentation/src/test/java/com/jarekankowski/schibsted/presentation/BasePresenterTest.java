package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipesUseCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public class BasePresenterTest {

    @Mock
    private RecipeView         mView;

    @Mock
    private UseCaseExecutor    mUseCaseExecutor;

    @Mock
    private LoadRecipesUseCase mLoadRecipesUseCase;

    private RecipePresenter    mRecipePresenter;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);
        mRecipePresenter = new RecipePresenter(mView, mUseCaseExecutor, mLoadRecipesUseCase);
    }

    @Test
    public void testOnDestroy()
            throws Exception {
        mRecipePresenter.onDestroy();

        verify(mUseCaseExecutor).unsubscribe();

        verifyNoMoreInteractions(mUseCaseExecutor);
    }

    @Test
    public void testOnSubscribe()
            throws Exception {

        mRecipePresenter.onSearchQueryFilled("query");

        verify(mUseCaseExecutor).subscribe(argThat(is(mLoadRecipesUseCase)), argThat(is(mRecipePresenter)));

        verifyNoMoreInteractions(mUseCaseExecutor);
    }
}