package com.jarekankowski.schibsted.presentation;

import com.jarekankowski.schibsted.data.db.model.Recipe;
import com.jarekankowski.schibsted.domain.UseCaseExecutor;
import com.jarekankowski.schibsted.domain.interaction.LoadRecipeDetailsUseCase;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by jarekankowski on 29/08/2016.
 */
public class RecipeDetailsPresenterTest {

    private static final String RECIPE_ID = "recipeId";

    @Mock
    private RecipeDetailsView        mView;

    @Mock
    private UseCaseExecutor          mUseCaseExecutor;

    @Mock
    private LoadRecipeDetailsUseCase mLoadRecipeDetailsUseCase;

    private RecipeDetailsPresenter   mRecipeDetailsPresenter;

    @Before
    public void setUp()
            throws Exception {
        MockitoAnnotations.initMocks(this);
        mRecipeDetailsPresenter = new RecipeDetailsPresenter(mView, mUseCaseExecutor, mLoadRecipeDetailsUseCase);
    }

    @Test
    public void testOnCreate()
            throws Exception {
        mRecipeDetailsPresenter.onCreate(RECIPE_ID);

        verify(mView).showLoader();

        verify(mLoadRecipeDetailsUseCase).setRecipeId(RECIPE_ID);

        verify(mUseCaseExecutor).subscribe(argThat(is(mLoadRecipeDetailsUseCase)), argThat(is(mRecipeDetailsPresenter)));

        verifyNoMoreInteractions(mView);
        verifyNoMoreInteractions(mLoadRecipeDetailsUseCase);
        verifyNoMoreInteractions(mUseCaseExecutor);
    }

    @Test
    public void testOnError()
            throws Exception {
        mRecipeDetailsPresenter.onError(new Exception());

        verify(mView).hideLoadingView();
        verify(mView).hideIngredientsView();

        verifyNoMoreInteractions(mView);
    }

    @Test
    public void testOnNext()
            throws Exception {
        final Recipe recipe = Mockito.mock(Recipe.class);
        mRecipeDetailsPresenter.onNext(recipe);

        verify(mView).hideLoadingView();
        verify(mView).updateRecipeDetails(recipe);

        verifyNoMoreInteractions(mView);
    }
}